import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Rentals } from './rentals';
import { Activities } from './activities';
import { Comments } from './comments';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createForms } from 'react-redux-form';
import { InitialFeedback }  from './forms';


export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            rentals: Rentals,
            comments: Comments,
            activities: Activities,
            ...createForms({
                feedback: InitialFeedback
            })
        }),
        applyMiddleware(thunk,logger)
    );

    return store;
}
