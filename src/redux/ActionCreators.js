import  * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});

export const postComment = (rentalId, rating, author, comment) => (dispatch) => {

    const newComment = {
        rentalId: rentalId,
        rating: rating,
        author: author,
        comment: comment
    };
    newComment.date = new Date().toISOString();

    return fetch(baseUrl + 'comments', {
        method: "POST",
        body: JSON.stringify(newComment),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => dispatch(addComment(response)))
    .catch(error =>  { console.log('post comments', error.message); alert('Your comment could not be posted\nError: '+error.message); });
};

export const postFeedback = (firstname, lastname, telnum, email, agree, contactType, message) => (dispatch) => {

    const newFeedback = {
        firstname:firstname,
        lastname:lastname,
        telnum:telnum,
        email:email,
        agree:agree,
        contactType:contactType,
        message:message
    };
    newFeedback.date = new Date().toISOString();

    return fetch(baseUrl + 'feedback',{
        method: "POST",
        body: JSON.stringify(newFeedback),
        headers:{
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
    .then(response =>{
        if(response.ok){
            return response;
        }else{
            var error = new Error('Error' + response.status + ': ' + response.statusText);
            error.response = response ;
            throw error;
        }
    },
    error => {
        throw error;
    })
    .then(response => response.json())
    .then(response => {alert('Thanks For your Feedback! ' + JSON.stringify(response))})
    .catch(error =>  { console.log('post Feedback', error.message); alert('Your Feedback could not be posted\nError: '+error.message); });
}


export const fetchRentals = () => (dispatch) => {
    dispatch(rentalsLoading(true));

    return fetch(baseUrl + 'rentals')
        .then(response => {
            if(response.ok) {
                return response;
            }else{
                var error = new Error('Error' + response.status +': '+response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json() )
        .then(rentals => dispatch(addRentals(rentals)))
        .catch(error => dispatch(rentalsFailed(error.message)));
}

export const rentalsLoading = () => ({
    type:ActionTypes.RENTALS_LOADING
});

export const rentalsFailed = (errmess) => ({
    type:ActionTypes.RENTALS_FAILED,
    payload:errmess
});

export const addRentals = (rentals) => ({
    type:ActionTypes.ADD_RENTALS,
    payload:rentals
});

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
        .then(response => {
            if(response.ok) {
                return response;
            }else{
                var error = new Error('Error' + response.status +': '+response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json() )
        .then(comments => dispatch(addComments(comments)))
        .catch(error => dispatch(commentsFailed(error.message)));
}

export const commentsFailed = (errmess) => ({
    type:ActionTypes.COMMENTS_FAILED,
    payload:errmess
});

export const addComments = (comments) => ({
    type:ActionTypes.ADD_COMMENTS,
    payload:comments
});


export const fetchActivities = () => (dispatch) => {

    dispatch(activitiesLoading());

    return fetch(baseUrl + 'activities')
        .then(response => {
            if(response.ok) {
                return response;
            }else{
                var error = new Error('Error' + response.status +': '+response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json() )
        .then(activities => dispatch(addActivities(activities)))
        .catch(error => dispatch(activitiesFailed(error.message)));
}

export const activitiesLoading = () => ({
    type: ActionTypes.PROMOS_LOADING
});

export const activitiesFailed = (errmess) => ({
    type:ActionTypes.ACTIVITIES_FAILED,
    payload:errmess
});

export const addActivities = (activities) => ({
    type:ActionTypes.ADD_ACTIVITIES,
    payload:activities
});
