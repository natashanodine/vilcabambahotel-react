import React from 'react';

function Footer(props) {
    return(
    <div className="footer">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-4 col-sm-4">
                        <div className="btn-group" role="group">
                            <a role="button" className="btn btn-primary" href="tel:+85212345678"><i className="fa fa-phone"></i> Call</a>
                            <a role="button" className="btn btn-info" href="tel:+85212345678"><i className="fa fa-skype"></i> Skype</a>
                            <a role="button" className="btn btn-success" href="mailto:vilcabambahotel@fvilcabambahotel.net"><i className="fa fa-envelope-o"></i> Email</a>
                        </div>
                </div>
               <div className="col-4 col-sm-4">
                    <h5>Our Address</h5>
                    <address>
		              <i className="fa fa-phone fa-lg"></i>: +593 991062762<br />
		              <i className="fa fa-envelope fa-lg"></i>: <a href="mailto:vilcabambahotel@fvilcabambahotel.net">
                         mailto:vilcabambahotel@fvilcabambahotel.net</a>
                    </address>
                </div>
                <div className="col-4 col-sm-4 align-self-center">
                    <div className="text-center">
                        <a className="btn btn-social-icon btn-facebook" href="www.google.com"><i className="fa fa-facebook"></i></a>
                        <a className="btn btn-social-icon btn-twitter" href="www.google.com"><i className="fa fa-twitter"></i></a>
                        <a className="btn btn-social-icon btn-google" href="www.google.com"><i className="fa fa-youtube"></i></a>
                        <a className="btn btn-social-icon" href="mailto:"><i className="fa fa-envelope-o"></i></a>
                    </div>
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col-auto">
                    <p>© Copyright 2019 Vilcabamba Hotel</p>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Footer;
