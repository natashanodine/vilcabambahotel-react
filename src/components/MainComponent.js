import React, { Component } from 'react';
import Menu from './MenuComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import {Switch, Route, Redirect, withRouter } from 'react-router-dom';
import RentalDetail from './RentalDetailComponent';
import Ecoactivity from './EcoactivityComponent';
import { connect } from 'react-redux';
import { postFeedback, postComment, fetchRentals, fetchComments, fetchActivities } from '../redux/ActionCreators';
import { actions } from 'react-redux-form';
import { TransitionGroup, CSSTransition } from 'react-transition-group';


const mapStateToProps = state => {
    return{
        rentals:state.rentals,
        comments:state.comments,
        activities:state.activities
    }
}

const mapDispatchToProps = (dispatch) => ({
    postComment: (rentalId, rating, author, comment) => dispatch(postComment(rentalId, rating, author, comment)),
    postFeedback: (firstname, lastname, telnum, email, agree, contactType, message) => dispatch(postFeedback(firstname, lastname, telnum, email, agree, contactType, message)),
    fetchRentals: () => {dispatch(fetchRentals())},
    resetFeedbackForm: () => {dispatch(actions.reset('feedback'))},
    fetchComments: () => dispatch(fetchComments()),
    fetchActivities: () => dispatch(fetchActivities())
});


class Main extends Component {
    
    componentDidMount(){
        this.props.fetchRentals();
        this.props.fetchComments();
        this.props.fetchActivities();
    }


  render() {
      const HomePage = () => {
          return(
              <Home rental={this.props.rentals.rentals.filter((rental) => rental.featured)[0]}
                    rentalsLoading={this.props.rentals.isLoading}
                    rentalsErrMess={this.props.rentals.errMess}
                    activity={this.props.activities.activities.filter((activity) => activity.featured)[0]}
                    activitiesLoading={this.props.activities.isLoading}
                    activitiesErrMess={this.props.activities.errMess}
              />
          );
      }

      const RentalWithId = ({match}) => {
          return(
              <RentalDetail rental={this.props.rentals.rentals.filter((rental) => rental.id === parseInt(match.params.rentalId,10))[0]}
                  isLoading={this.props.rentals.isLoading}
                  errMess={this.props.rentals.errMess}
                  comments={this.props.comments.comments.filter((comment) => comment.rentalId === parseInt(match.params.rentalId,10))}
                  commentsErrMess={this.props.comments.errMess}
                  postComment={this.props.postComment} />
          );
      }

    return (
      <div>
        <Header />
        <TransitionGroup>
            <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
                <Switch location={this.props.location} >
                    <Route path="/home" component={HomePage} />
                    <Route exact path="/menu" component={() => <Menu rentals={this.props.rentals} /> } />
                    <Route path="/menu/:rentalId" component={RentalWithId} />
                    <Route exact path='/contactus' component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm} postFeedback={this.props.postFeedback} />} />} />
                    <Route exact path='/ecoactivity' component={() => <Ecoactivity activities={this.props.activities} activitiesLoading={this.props.activities.isLoading} activityErrMess={this.props.activities.errMess} /> } />
                    <Redirect to="/home" />
                </Switch>
            </CSSTransition>
        </TransitionGroup>
        <Footer />
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
