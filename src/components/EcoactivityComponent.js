import React from 'react';
import { Breadcrumb, BreadcrumbItem, Media } from 'reactstrap';
import { Link } from 'react-router-dom';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import { Fade,Stagger } from 'react-animation-components';


    function RenderActivity({activity}){
        return(
            <Media key={activity.id} tag="li">
                <Media left middle>
                    <Media object src={baseUrl + activity.image} alt={activity.name} />
                </Media>
                <Media body className="ml-5">
                    <Media heading>{activity.name}</Media>
                    <p>{activity.designation}</p>
                    <p>{activity.description}</p>
                    <br/>
                </Media>
            </Media>
        );
    }


    function Activities(props) {

        if (props.activities.isLoading) {
            return( <Loading /> )
        }

        else if (props.activities.errMess) {
            return( <h4>{props.errMess}</h4> )
        }

        else {
            const activities = props.activities.activities.map((activity) => {
                return (
                    <Fade in key={activity.id}>
                        <RenderActivity activity={activity} />
                    </Fade>
                );
            });

            return (
                <Stagger in>
                {activities}
                </Stagger>
            )
        }
    }


function Ecoactivity(props) {

    return(
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Ecoactivity Us</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Ecoactivities</h3>
                    <hr />
                </div>
            </div>
			
            <div className="row row-content">
                <div className="col-12">
                    <br/>
                    <Media list>
                       <Activities activities={props.activities} isLoading={props.activitiesLoading} errMess={props.activityErrMess} />
                    </Media>
                </div>
            </div>
        </div>
    );
}

export default Ecoactivity;
