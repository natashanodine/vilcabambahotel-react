import React from 'react';
import { Card, CardImg, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform } from 'react-animation-components';

import { Jumbotron } from 'reactstrap';

function RenderCard({item, isLoading, errMess}){
    if(isLoading){
        return(
            <Loading />
        );
    }
    else if(errMess){
        return(
            <h4>{errMess}</h4>
        );
    }
    else {
        return(
            <FadeTransform in transformProps={{ exitTransform:'scale(0.5) translateY(-50%)' }}>
                <Card>
                    <CardImg src={baseUrl + item.image} alt={item.name} />
                    <CardBody>
                        <CardTitle>{item.name}</CardTitle>
                        { item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
                    </CardBody>
                </Card>
            </FadeTransform>
        );
    }
}


function Home(props){
    return(
        <div className="container-fluid">
			<Jumbotron>
				<div className="row index_intro">
					<div className="col-12 col-md-6 logo_index">
						<img alt="vilcabamba hotel logo" src ="http://vilcabamba-hotel.com/images/logo.gif" />
					</div>
					<div className="col-12 col-md-6 introduction">
						<p><i className="fa fa-quote-left"></i>
						Cabañas Rio Yambala are located at the start of the trail to the Podocarpus National Park & the El Palto waterfall. You can hike the local trails that lead to swim holes along the river,  local beauty spots and into the heart of the mountains, the edge of the cloud forest and the Podocarpus National Park or enjoy the panoramic views, the birds and the sounds of nature from the comfort of your hammock.
						<br/>Long standing recommendations from popular travel guides. Excellent reviews on Airbnb, Booking & Trip Advisor
						<i className="fa fa-quote-right"></i></p>
						
					</div>
				</div>
            </Jumbotron>
            <div className="row align-items-start">
                <div className="col-12 col-md m-1">
                    <RenderCard item={props.rental} isLoading={props.rentalsLoading} errMess={props.rentalsErrMess} />
                </div>
                <div className="col-12 col-md m-1">
                    <RenderCard item={props.activity} isLoading={props.activitiesLoading} errMess={props.activitiesErrMess} />
                </div>
            </div>
        </div>
    );
}

export default Home;
