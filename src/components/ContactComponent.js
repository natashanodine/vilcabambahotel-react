import React, {Component} from 'react';
import { Breadcrumb,BreadcrumbItem,Button,Col,Label,Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, Form, Errors } from 'react-redux-form';


const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);
const isNumber = (val) => !isNaN(Number(val));
const validEmail = (val) => /^[A-Z0-9.-_+%]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);



class Contact extends Component {

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(values){
        console.log("Current state: "+ JSON.stringify(values));
        // alert("Current state: "+ JSON.stringify(values));
        this.props.postFeedback(values.firstname, values.lastname, values.telnum, values.email, values.agree, values.contactType, values.message);
        this.props.resetFeedbackForm();
    }


    render(){
        return(
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Contact Us</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Contact Us</h3>
                        <hr />
                    </div>
                </div>
                <div className="row row-content">
                    <div className="col-12 col-sm-6">
                            <h5>Send Message</h5>
                             <Form model="feedback" onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="firstname" md={2}>First Name</Label>
                                <Col md={10}>
                                    <Control.text model=".firstname" className="form-control" id="firstname" name="firstname" placeholder="First Name" validators={{
                                            required,minLength:minLength(3),maxLength:maxLength(15)
                                        }} />
                                    <Errors className="text-danger" model=".firstname" show="touched" messages={{
                                            required:'Required',
                                            minLength:'Must be greater than 2 characters',
                                            maxLength:'Must be 15 characters or less'
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="lastname" md={2}>Last Name</Label>
                                <Col md={10}>
                                    <Control.text model=".lastname" className="form-control" id="lastname" name="lastname" placeholder="Last Name" validators={{
                                            required,minLength:minLength(3),maxLength:maxLength(15)
                                        }}/>
                                    <Errors className="text-danger" model=".lastname" show="touched" messages={{
                                            required:'Required',
                                            minLength:'Must be greater than 2 characters',
                                            maxLength:'Must be 15 characters or less'
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="telnum" md={2}>Contact Tel.</Label>
                                <Col md={10}>
                                    <Control.text model=".telnum" className="form-control" id="telnum" name="telnum" placeholder="Contact Tel." validators={{
                                            required, minLength: minLength(3), maxLength: maxLength(15), isNumber
                                        }} />
                                    <Errors className="text-danger" model=".telnum" show="touched" messages={{
                                            required:'Required',
                                            minLength:'Must be greater than 2 characters',
                                            maxLength:'Must be 15 characters or less',
                                            isNumber: 'Must be a number'
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="email" md={2}>Email</Label>
                                <Col md={10}>
                                    <Control.text model=".email" className="form-control" id="email" name="email" placeholder="Email" validators={{
                                            required, validEmail
                                        }} />
                                    <Errors className="text-danger" model=".email" show="touched" messages={{
                                            required:'Required',
                                            validEmail: 'Invalid Email Address'
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{size: 6, offset: 2}}>
                                    <div className="form-check">
                                        <Label check>
                                            <Control.checkbox model=".agree" className="form-check-input" id="agree" name="agree"  /> {' '}
                                            <strong>May we contact you?</strong>
                                        </Label>
                                    </div>
                                </Col>
                                <Col md={{ size:3, offset:1 }} >
                                    <Control.select model=".contactType" className="form-control" name="contactType" id="contactType" >
                                        <option>Tel.</option>
                                        <option>Email</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="message" md={2}>Your Feedback</Label>
                                <Col md={10}>
                                    <Control.textarea model=".message" id="message" name="message" rows="12" className="form-control" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{size:10, offset:2}}>
                                    <Button type="submit" color="primary">Send Feedback</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                    <div className="col-12 col-sm-6 ">
                        <h5>Information</h5>
						
						<p>From Vilcabamba get a taxi to "Cabañas Rio Yambala", also known as "las cabañas de Charlie".</p>
						<p>- The Taxi will drop you at our bridge. Cross the bridge, turn right and follow the signs up to our house and the cabins. It is a 3-4 minute uphill walk.</p>
						<p>- If you call us on <a href="tel:+593991062762">0991062762</a> or WhatsApp us on <a href="tel:+593939953585">0939953585</a> just before leaving Vilcabamba we can come down and meet you at the bridge.</p>
						<p>- If you arrive after dark or want help bringing your luggage up then you should definitely call first.</p>

						<p>There are plenty of stores and restaurants in Vilcabamba but not much further out, so it is advisable to eat or get a few groceries before coming up if you are arriving late.</p>

						<p>From AIRPORT/LOJA to CABINS</p>
						<p>We can send a taxi (at cost) to pick you up in Loja or at the Loja airport</p>
							</div>
                </div>
                <div className="row row-content">
                    <div className="col-12">
					
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact;
