import React  from 'react';
import { Card,CardImg,CardImgOverlay,CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';


    function RenderMenuItem({rental , onClick }){
        return(
            <Card>
                <Link to={`/menu/${rental.id}`} >
                    <CardImg width="100%" src={baseUrl + rental.image} alt={rental.name} />
                    <CardImgOverlay>
                        <CardTitle>{rental.name}</CardTitle>
                    </CardImgOverlay>
                </Link>
            </Card>
        );
    }



    const Menu = (props) => {
        const menu = props.rentals.rentals.map((rental) => {
            return (
              <div key={rental.id} className="col-12 col-md-5 m-1">
                <RenderMenuItem rental={rental} />
              </div>
            );
        });

        if(props.rentals.isLoading){
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.rentals.errMess){
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.rentals.errMess}</h4>
                    </div>
                </div>
            );
        }
        else{
            return(
                <div className="container">
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Rentals</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>Rentals</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        {menu}
                    </div>
                </div>
            );
        }
    }

export default Menu;
